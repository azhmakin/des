//
// Implementation of the DES algorithm
// By Andrey Zhmakin, 2021
//

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstdlib>

//
// Sources:
//
// Understanding Cryptography: A Textbook for Students and Practitioners (2010) by Christof Paar & Jan Pelzl
// Chapter 3: The Data Encryption Standard (DES) and Alternatives, In particular p.58-72
//
// Lecture 5: Data Encryption Standard (DES): Encryption by Christof Paar
// https://www.youtube.com/watch?v=kPBJIhpcZgE
//
// Lecture 6: Data Encryption Standard (DES): Key Schedule and Decryption by Christof Paar
// https://www.youtube.com/watch?v=l-7YW06BFNs
//
// The DES Algorithm Illustrated by J. Orlin Grabbe
// http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm
//


typedef uint64_t uint_least48_t;


// Initial permutation
uint8_t IP[64] = {
    58, 50, 42, 34, 26, 18, 10, 2,
    60, 52, 44, 36, 28, 20, 12, 4,
    62, 54, 46, 38, 30, 22, 14, 6,
    64, 56, 48, 40, 32, 24, 16, 8,
    57, 49, 41, 33, 25, 17,  9, 1,
    59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5,
    63, 55, 47, 39, 31, 23, 15, 7
};

// Final permutation, inverse of IP, i.e. IP^-1
uint8_t IP_1[64] = {
    40, 8, 48, 16, 56, 24, 64, 32,
    39, 7, 47, 15, 55, 23, 63, 31,
    38, 6, 46, 14, 54, 22, 62, 30,
    37, 5, 45, 13, 53, 21, 61, 29,
    36, 4, 44, 12, 52, 20, 60, 28,
    35, 3, 43, 11, 51, 19, 59, 27,
    34, 2, 42, 10, 50, 18, 58, 26,
    33, 1, 41,  9, 49, 17, 57, 25
};

uint8_t E[48] = {
    32,  1,  2,  3,  4,  5,
     4,  5,  6,  7,  8,  9,
     8,  9, 10, 11, 12, 13,
    12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21,
    20, 21, 22, 23, 24, 25,
    24, 25, 26, 27, 28, 29,
    28, 29, 30, 31, 32,  1
};

uint8_t P[32] = {
    16,  7, 20, 21, 29, 12, 28, 17,
     1, 15, 23, 26,  5, 18, 31, 10,
     2,  8, 24, 14, 32, 27,  3,  9,
    19, 13, 30,  6, 22, 11,  4, 25
};

// S-boxes
uint8_t S[8][64] = {
    {
        14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
         0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
         4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
        15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13
    },
    {
        15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
         3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
         0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
        13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9
    },
    {
        10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
        13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
        13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
         1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12
    },
    {
         7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
        13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
        10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
         3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14
    },
    {
         2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9,
        14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6,
         4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14,
        11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3
    },
    {
        12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
        10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
         9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
         4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13
    },
    {
         4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
        13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
         1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
         6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12
    },
    {
        13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
         1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
         7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
         2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11
    }
};

// Used in key schedule
// Drop parity bits from the 64-bit key representation, permutate.
uint8_t PC_1[56] = {
    57, 49, 41, 33, 25, 17,  9,  1,
    58, 50, 42, 34, 26, 18, 10,  2,
    59, 51, 43, 35, 27, 19, 11,  3,
    60, 52, 44, 36, 63, 55, 47, 39,
    31, 23, 15,  7, 62, 54, 46, 38,
    30, 22, 14,  6, 61, 53, 45, 37,
    29, 21, 13,  5, 28, 20, 12,  4
};

uint8_t PC_2[48] = {
    14, 17, 11, 24,  1,  5,  3, 28,
    15,  6, 21, 10, 23, 19, 12,  4,
    26,  8, 16,  7, 27, 20, 13,  2,
    41, 52, 31, 37, 47, 55, 30, 40,
    51, 45, 33, 48, 44, 49, 39, 56,
    34, 53, 46, 42, 50, 36, 29, 32
};



void print_bits_be(uint64_t x, int n, int group = 8)
{
    for (int i = n, count = 1; --i >= 0; count++)
    {
        std::cout << ((x >> i) & 1);

        if (count % group == 0)
        {
            std::cout << ' ';
        }
    }

    std::cout << '\n';
}


uint64_t permutate(uint64_t x, uint8_t p[], int from_bits, int to_bits)
{
    uint64_t res = 0;

    for (int i = 0; i < to_bits; i++)
    {
        if (((x >> (from_bits - p[i])) & 1) == 1)
        {
            res |= UINT64_C(1) << (to_bits - 1 - i);
        }
    }

    return res;
}


inline
uint32_t left(uint64_t value)
{
    return value >> 32;
}


inline
uint32_t right(uint64_t value)
{
    return (uint32_t)value;
}


inline
uint64_t left_and_right(uint32_t left, uint32_t right)
{
    return (uint64_t) right | (uint64_t) left << 32;
}


inline
uint64_t swap_left_and_right(uint64_t r)
{
    return (r >> 32) | (r << 32);
}


inline
uint32_t s_box(int i, int value)
{
    int row = ((value >> 4) & 0b10) | (value & 1);
    int col = (value >> 1) & 0b1111;

    int index = row * 16 + col;

    return S[i][index];
}


//
// https://www.youtube.com/watch?v=kPBJIhpcZgE
// 55:55
// Book p.62 (77/382)
//
uint32_t f(uint32_t r0, uint_least48_t subkey)
{
    uint_least48_t r = permutate(r0, E, 32, 48);

    //std::cout << "E(R0): "; print_bits_be(r, 48, 6);
    //std::cout << "EXP  : 011110 100001 010101 010101 011110 100001 010101 010101\n\n";

    //std::cout << "K1: "; print_bits_be(subkey, 48, 6);

    r ^= subkey;
    //std::cout << "X1: "; print_bits_be(r, 48, 6);

    uint32_t result = 0;

   // std::cout << "S-boxes: \n";
    //std::cout << "Expected:  0101 1100 1000 0010 1011 0101 1001 0111 " << std::endl;

    for (int i = 0; i < 8; i++)
    {
        int value = (r >> ((7 - i) * 6)) & 0b111111;

        uint32_t s = s_box(i, value);

        //print_bits_be(value, 6, 6);
        //print_bits_be(s, 4, 4);
        //std::cout << "\n";

        result |= s << ((7 - i) * 4);
    }

    //std::cout << "Res of S-boxes: "; print_bits_be(result, 32, 4);
    //std::cout << "\n\n";

    uint32_t res = (uint32_t) permutate(result, P, 32, 32);

    //std::cout << "Res: "; print_bits_be(res, 32, 4);

    return res;
}


inline
uint64_t des_round(uint64_t r, uint_least48_t subkey)
{
    uint32_t l0 = left(r);
    uint32_t r0 = right(r);

    //std::cout << "L0: "; print_bits_be(l0, 32, 4);
    //std::cout << "R0: "; print_bits_be(r0, 32, 4);
    //std::cout << std::endl;

    uint32_t l1 = r0;
    uint32_t r1 = l0 ^ f(r0, subkey);

    //std::cout << "L1: "; print_bits_be(l1, 32, 4);
    //std::cout << "R1: "; print_bits_be(r1, 32, 4);

    //std::cout << "EXP:1110 1111 0100 1010 0110 0101 0100 0100 " << std::endl;

    //std::cout << std::hex << std::setfill('0') << std::setw(8) << l1 << " " << std::hex << std::setfill('0') << std::setw(8) << r1 << std::endl;

    return left_and_right(l1, r1);
}


/**
 * \param Pt plaintext, 64 bits.
 * \param subkey An array of 16 48-bit words. One for each round.
 */
uint64_t des_encrypt(uint64_t Pt, uint_least48_t subkey[])
{
    uint64_t r = permutate(Pt, IP, 64, 64);

    for (int i = 0; i < 16; i++)
    {
        r = des_round(r, subkey[i]);
    }

    r = swap_left_and_right(r);

    return permutate(r, IP_1, 64, 64);
}


uint64_t des_decrypt(uint64_t Ct, uint_least48_t subkey[])
{
    uint64_t r = permutate(Ct, IP, 64, 64);

    for (int i = 16; --i >= 0; )
    {
        r = des_round(r, subkey[i]);
    }

    r = swap_left_and_right(r);

    return permutate(r, IP_1, 64, 64);
}


inline
uint32_t left_rotate_28(uint32_t value, int n)
{
    return (
            (value << n) | ((value >> (28 - n)) & 0xFFFFFFF)
        ) & 0xFFFFFFF;
}


/**
 * \param key Key.
 * \param subkey Sixteen 48-bit words.
 */
void des_key_schedule(uint64_t key, uint_least48_t subkey[])
{
    //std::cout << "K: "; print_bits_be(key, 64);

    uint64_t t = permutate(key, PC_1, 64, 56);

    //std::cout << "K+: ";  print_bits_be(t, 56, 7);
    //std::cout << "P : 1111000 0110011 0010101 0101111 0101010 1011001 1001111 0001111 " << std::endl;

    uint32_t c = (t >> 28) & 0xFFFFFFF,
             d = t & 0xFFFFFFF;

    //std::cout << "C0: ";  print_bits_be(c, 28, 7);
    //std::cout << "D0: ";  print_bits_be(d, 28, 7);

    for (int i = 1; i <= 16; i++)
    {
        int rotation = (i == 1 || i == 2 || i == 9 || i == 16) ? 1 : 2;

        c = left_rotate_28(c, rotation);
        d = left_rotate_28(d, rotation);

        //std::cout << "c" << i << ": "; print_bits_be(c, 28, 7);
        //std::cout << "d" << i << ": "; print_bits_be(d, 28, 7);

        uint64_t t = (uint64_t) d | ((uint64_t) c << 28);

        t = permutate(t, PC_2, 56, 48);

        subkey[i - 1] = t;

        //std::cout << "K" << i << ": "; print_bits_be(t, 48, 6);
        //std::cout << std::endl;
    }
}


/**
 * Checks odd parity bits of the 64-bit key representation.
 */
bool des_is_valid_key(uint64_t key)
{
    int i = 0, j = 0;
    int s = 0;

    while (i < 64)
    {
        int b = (key >> i) & 1;

        if ((i + 1) % 8 == 0)
        {
            if ((s & 1) == b) // Odd parity bit
            {
                return false;
            }

            s = 0;
            i++;
        }
        else
        {
            s ^= b;

            i++;
            j++;
        }
    }

    return true;
}


/**
 * Encode 7 bytes little-endian (56 bits) as a DES key.
 * \param key 7 bytes of a key.
 * \return Key with parity bits.
 */
uint64_t des_encode_key(uint8_t key[])
{
    uint64_t result = 0;

    int index = 0;
    int sum = 1;

    for (int i = 0; i < 7; i++) // Iterate over bytes
    {
        for (int j = 0; j < 8; j++) // Iterate over bits in a byte
        {
            int bit = (key[i] >> j) & 1;

            if (bit != 0)
            {
                sum++;
                result |= UINT64_C(1) << index;
            }

            index++;

            if ((index + 1) % 8 == 0)
            {
                result |= (sum & UINT64_C(1)) << index;
                sum = 1;
                index++;
            }
        }
    }

    return result;
}


/**
 * Convert a 64-bit key representation into a 7-byte little-endian representation.
 * \param encoded 64-bit encoded representation of the key.
 * \param key Seven-byte key without parity bits.
 * \return True, if parity check succeeds. 
 */
bool des_decode_key(uint64_t encoded, uint8_t key[])
{
    bool ok = true;

    int j = 0;
    int k = -1;
    int s = 0;

    for (int i = 0; i < 64; i++)
    {
        int b = (encoded >> i) & 1;

        if ((i + 1) % 8 == 0)
        {
            if ((s & 1) == b) // Odd parity bit
            {
                ok = false;
            }

            s = 0;
        }
        else
        {
            s ^= b;

            int index = j / 8;

            if (index > k)
            {
                key[index] = b;
                k = index;
            }
            else
            {
                key[index] |= b << (j % 8);
            }

            j++;
        }
    }

    return ok;
}


void run_against_test_vectors()
{
    std::string line;
    std::ifstream myfile;
    myfile.open("test-vectors.txt");

    if (!myfile.is_open())
    {
        perror("Error open");
        exit(EXIT_FAILURE);
    }

    getline(myfile, line);

    while (getline(myfile, line))
    {
        if (line.empty())
        {
            break;
        }

        std::string strKey = line.substr(0, 16);
        std::string strPlain = line.substr(17, 16);
        std::string strCipher = line.substr(34, 16);

        //cout << strKey << " " << strPlain << " " << strCipher << endl;
        //exit(0);

        uint64_t key0 = strtoull(strKey.c_str(), nullptr, 16);
        uint64_t plain0 = strtoull(strPlain.c_str(), nullptr, 16);
        uint64_t cipher0 = strtoull(strCipher.c_str(), nullptr, 16);

        uint_least48_t K[16];
        des_key_schedule(key0, K);

        uint64_t cipher1 = des_encrypt(plain0, K);
        uint64_t plain1 = des_decrypt(cipher0, K);

        std::cout << std::hex << std::setfill('0') << std::setw(16) << key0 << " "
            << std::hex << std::setfill('0') << std::setw(16) << plain0 << " "
            << std::hex << std::setfill('0') << std::setw(16) << cipher0;

        std::cout << (des_is_valid_key(key0) ? " KEY-VALID" : " KEY-INVALID");

        uint8_t keyBytes[7];
        des_decode_key(key0, keyBytes);
        std::cout << ((key0 == des_encode_key(keyBytes)) ? " KEY-ENCODING-OK" : " KEY-ENCODING-ERROR");

        if (cipher1 != cipher0)
        {
            std::cout << " " << std::hex << std::setfill('0') << std::setw(16) << cipher1;
        }
        else
        {
            std::cout << " ENCRYPT-OK";
        }

        if (plain1 != plain0)
        {
            std::cout << " " << std::hex << std::setfill('0') << std::setw(16) << cipher1;
        }
        else
        {
            std::cout << " DECRYPT-OK";
        }

        std::cout << std::endl;
    }

    myfile.close();
}


void print_byte(uint8_t byte)
{
    for (int i = 8; --i >= 0; )
    {
        printf("%c", ((byte >> i) & 1) ? '1' : '0');
    }
}


void key_transcoding()
{
    uint64_t key0 = UINT64_C(0x0170F175468FB5E6);
    print_bits_be(key0, 64, 8);

    uint8_t bytes[7];

    des_decode_key(key0, bytes);
    for (int i = 7; --i >= 0; )
    {
        print_byte(bytes[i]);
        printf(" ");
    }
    printf("\n");

    uint64_t key1 = des_encode_key(bytes);
    print_bits_be(key1, 64, 8);
}


int main()
{
    run_against_test_vectors();
    //key_transcoding();

    // Documentation, p.9
    // Key: 10316E028C8F3B4A
    // Pt : 0000000000000000
    // Ct : 82DCBAFBDEAB6602
    //uint64_t Ky = UINT64_C(0x10316E028C8F3B4A);
    //uint64_t Pt = UINT64_C(0x0000000000000000);

    // Applied Cryptography sample
    // Key: 0123456789abcdef
    // Pt : 0123456789abcde7
    // Ct : c9 57 44 25 6a 5e d3 1d
    //uint64_t Ky = UINT64_C(0x0123456789abcdef);
    //uint64_t Pt = UINT64_C(0x0123456789abcde7);

    // Documentation, 1st test vector
    // Key: 0101010101010101
    // Pt : 95F8A5E5DD31D900
    // Ct : 8000000000000000
    //uint64_t Ky = UINT64_C(0x0101010101010101);
    //uint64_t Pt = UINT64_C(0x95F8A5E5DD31D900);

    // Grabbe
    // Key: 133457799BBCDFF1
    // Pt : 0123456789ABCDEF
    // Ct : 85E813540F0AB405
    //uint64_t Ky = UINT64_C(0x133457799BBCDFF1);
    //uint64_t Pt = UINT64_C(0x0123456789ABCDEF);
    //uint64_t Ct = UINT64_C(0x85E813540F0AB405);

    //uint_least48_t K[16];
    //des_key_schedule(Ky, K);
    
    //uint64_t k = from_binary("00010011 00110100 01010111 01111001 10011011 10111100 11011111 11110001");
    //std::cout << std::hex << k << std::endl;
    //des_key_schedule(k, K);

    //uint64_t m = from_binary("0000 0001 0010 0011 0100 0101 0110 0111 1000 1001 1010 1011 1100 1101 1110 1111");

    //std::cout << std::hex << des_encrypt(UINT64_C(0x1234567890ABCDEF), K);
    //std::cout << std::hex << std::setfill('0') << std::setw(16) << des_encrypt(Pt, K) << std::endl; ////////////////////////////////////////////////////////////////////////////////////////
    //std::cout << std::hex << std::setfill('0') << std::setw(16) << des_decrypt(Ct, K) << std::endl;
    //std::cout << std::hex << 0x85E813540F0AB405UL << endl;
    //std::cout << std::hex << des_encrypt(m, K);

    //uint64_t n = des_encrypt(m, K);
    //cout << "Result: ";  print_bits_be(n, 64, 8);

    //std::cout << des_is_valid_key(UINT64_C(0x10316E028C8F3B4A)) << '\n';

    /*
   */
}


// Squeeze 64 bit key to 56 bits.
/*uint64_t squeeze(uint64_t key)
{
    int i = 0, j = 0;
    uint64_t res = 0;

    for (; i < 64; i++)
    {
        if ((i + 1) % 8 == 0)
        {
            continue;
        }

        if ((key >> i) & 1)
        {
            res |= UINT64_C(1) << j;
        }

        j++;
    }

    return res;
}*/

/*
uint64_t from_binary(const char *s)
{
    uint64_t res = 0;

    int i = 0, j = 0;

    for (; s[i] != '\0'; i++)
    {
        char c = s[i];

        if (c == '1')
        {
            res |= UINT64_C(1) << j;
            j++;
        }
        else if (c == '0')
        {
            j++;
        }
        else
        {

        }
    }

    return res;
}
*/
